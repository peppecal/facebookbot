from flask import Flask, request
from pymessenger.bot import Bot
from .secret import access_token, verify_token

app = Flask(__name__)
bot = Bot(access_token)


@app.route("/webhook", methods=['POST', 'GET'])
def webhook():
    if request.method == 'GET':
        token_sent = request.args.get("hub.verify_token")
        if token_sent == verify_token:
            return request.args.get("hub.challenge")
        else:
            return "No."
    else: # It's a POST request, or something strange
        output = request.get_json()
        for event in output['entry']:
            for message in event['messaging']:
                if message.get('message'):
                    recipient_id = message['sender']['id']
                    bot.send_text_message(recipient_id, "Non ti leggo, ma ti rispondo ignorandoti.")
        return ""
